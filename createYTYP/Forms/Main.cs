﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace createYTYP
{
    public partial class Main : Form
    {
        public Main()
        {
            InitializeComponent();
        }

        private void odrBrowserToolStripMenuItem_Click(object sender, EventArgs e)
        {
            odrbrowser f = new odrbrowser();
            f.Show();
            //this.Enabled = false;
        }

        private void yTYPCreatorToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Create f = new Create();
            f.Show();
            //this.Enabled = false;
        }

        private void convertYmapToXmlToolStripMenuItem_Click(object sender, EventArgs e)
        {
            convertYmap f = new convertYmap();
            f.Show();
            //this.Enabled = false;
        }

        private void viewYTYPsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            editYTYP f = new editYTYP();
            f.Show();
            //this.Enabled = false;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            Create f = new Create();
            f.Show();
            //this.Enabled = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            editYTYP f = new editYTYP();
            f.file_TB.Text = "";
            f.Show();
            //this.Enabled = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            convertYmap f = new convertYmap();
            f.Show();
            //this.Enabled = false;
        }
    }
}
