﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;
using System.IO;

namespace createYTYP
{
    public partial class Create : Form
    {
        public Create()
        {
            InitializeComponent();
        }

        public static XmlDocument tempDoc = new XmlDocument();

        //for importing BB's
        public static string I_bbminName;
        public static string I_bbminX;
        public static string I_bbminY;
        public static string I_bbminZ;

        public static string I_bbmaxName;
        public static string I_bbmaxX;
        public static string I_bbmaxY;
        public static string I_bbmaxZ;

        public static string I_centerName;
        public static string I_centerX;
        public static string I_centerY;
        public static string I_centerZ;

        public static string I_radiusName;
        public static string I_radius;

        public static string I_modelName;

        public static string lodDist = "0";
        public static string flags = "0";
        public static string specialAttributes = "0";
        public static string hdTextureDist = "0";
        public static string name = "null";
        public static string textureDictionaryName = "0";
        public static string textureDictionary = "0";
        public static string physicsDictionaryName = "0";
        public static string physicsDictionary = "0";
        public static string assetType = "0";

        public static bool isUsingTextureDict = false;
        public static bool isUsingDrawableDict = false;
        public static bool isUsingPhysicsDict = false;
        public static bool isUsingFileName = false;

        public static string fileName = "template";
        public static string filePathSavedTo = "";
        public static bool createdYtyp = false;
        public static bool isfn = false;

        //SavesXML
        private void SaveXML()
        {
            tempDoc.Save(Application.StartupPath + "\\" + fileName + ".ytyp.xml");

            filePathSavedTo = Application.StartupPath + "\\" + fileName + ".ytyp.xml";
            createdYtyp = true;
            ShowViewCreatedYtypButton();

            MessageBox.Show("Successfully created " + fileName + ".ytyp!", "Success");
        }

        //Creates item node with all data from form
        private void CreateItemNode()
        {
            //xmlnode paths
            XmlNode itemNodeLodDist = tempDoc.SelectSingleNode("CMapTypes/archetypes/Item/lodDist");
            XmlNode itemNodeFlags = tempDoc.SelectSingleNode("CMapTypes/archetypes/Item/flags");
            XmlNode itemNodeSpecialAttributes = tempDoc.SelectSingleNode("CMapTypes/archetypes/Item/specialAttribute");
            XmlNode itemNodebbMin = tempDoc.SelectSingleNode("CMapTypes/archetypes/Item/bbMin");
            XmlNode itemNodebbMax = tempDoc.SelectSingleNode("CMapTypes/archetypes/Item/bbMax");
            XmlNode itemNodebsCentre = tempDoc.SelectSingleNode("CMapTypes/archetypes/Item/bsCentre");
            XmlNode itemNodebsRadius = tempDoc.SelectSingleNode("CMapTypes/archetypes/Item/bsRadius");
            XmlNode itemNodehdTextureDist = tempDoc.SelectSingleNode("CMapTypes/archetypes/Item/hdTextureDist");
            XmlNode itemNodeName = tempDoc.SelectSingleNode("CMapTypes/archetypes/Item/name");
            XmlNode itemNodetextureDictionary = tempDoc.SelectSingleNode("CMapTypes/archetypes/Item/textureDictionary");
            XmlNode itemNodeDrawableDictionary = tempDoc.SelectSingleNode("CMapTypes/archetypes/Item/drawableDictionary");
            XmlNode itemNodePhysicsDictionary = tempDoc.SelectSingleNode("CMapTypes/archetypes/Item/physicsDictionary");
            XmlNode itemNodeAssetType = tempDoc.SelectSingleNode("CMapTypes/archetypes/Item/assetType");
            XmlNode itemNodeAssetName = tempDoc.SelectSingleNode("CMapTypes/archetypes/Item/assetName");
            XmlNode CmapNodeName = tempDoc.SelectSingleNode("CMapTypes/name");
            XmlNode itemNodeCreatedBy = tempDoc.SelectSingleNode("CMapTypes/CreatedBy");

            //lodDist
            var val1 = tempDoc.CreateAttribute("value");
            val1.Value = lodDist;
            itemNodeLodDist.Attributes.Append(val1);

            //flags
            var val2 = tempDoc.CreateAttribute("value");
            val2.Value = flags;
            itemNodeFlags.Attributes.Append(val2);

            //specialAttributes
            var val3 = tempDoc.CreateAttribute("value");
            val3.Value = specialAttributes;
            itemNodeSpecialAttributes.Attributes.Append(val3);

            //bbMin
            var bbX1 = tempDoc.CreateAttribute("x");
            bbX1.Value = I_bbminX;
            itemNodebbMin.Attributes.Append(bbX1);
            var bbY1 = tempDoc.CreateAttribute("y");
            bbY1.Value = I_bbminY;
            itemNodebbMin.Attributes.Append(bbY1);
            var bbZ1 = tempDoc.CreateAttribute("z");
            bbZ1.Value = I_bbminZ;
            itemNodebbMin.Attributes.Append(bbZ1);

            //bbMax
            var bbX2 = tempDoc.CreateAttribute("x");
            bbX2.Value = I_bbmaxX;
            itemNodebbMax.Attributes.Append(bbX2);
            var bbY2 = tempDoc.CreateAttribute("y");
            bbY2.Value = I_bbmaxY;
            itemNodebbMax.Attributes.Append(bbY2);
            var bbZ2 = tempDoc.CreateAttribute("z");
            bbZ2.Value = I_bbmaxZ;
            itemNodebbMax.Attributes.Append(bbZ2);

            //bsCentre
            var bsX1 = tempDoc.CreateAttribute("x");
            bsX1.Value = I_centerX;
            itemNodebsCentre.Attributes.Append(bsX1);
            var bsY1 = tempDoc.CreateAttribute("y");
            bsY1.Value = I_centerY;
            itemNodebsCentre.Attributes.Append(bsY1);
            var bsZ1 = tempDoc.CreateAttribute("z");
            bsZ1.Value = I_centerZ;
            itemNodebsCentre.Attributes.Append(bsZ1);

            //bsRadius
            var val4 = tempDoc.CreateAttribute("value");
            val4.Value = I_radius;
            itemNodebsRadius.Attributes.Append(val4);

            //hdTextureDist
            var val5 = tempDoc.CreateAttribute("value");
            val5.Value = hdTextureDist;
            itemNodehdTextureDist.Attributes.Append(val5);

            //name
            itemNodeName.InnerText = name;

            //textureDictionary
            if (isUsingTextureDict == true)
            {
                itemNodetextureDictionary.InnerText = textureDictionary_TB.Text;
            }
            else if (isUsingTextureDict == false)
            {
                itemNodetextureDictionary.InnerText = name;
            }

            //textureDictionary
            if (isUsingDrawableDict == true)
            {
                itemNodeDrawableDictionary.InnerText = drawableDictionary_TB.Text;
            }
            else if (isUsingDrawableDict == false)
            {

            }

            //physicsDictionary
            if (isUsingPhysicsDict == true)
            {
                itemNodePhysicsDictionary.InnerText = name;
            }
            else if(isUsingPhysicsDict == false)
            {
                if(embeddedCols_TB.Text.Length > 0)
                {
                    itemNodePhysicsDictionary.InnerText = embeddedCols_TB.Text;
                }
            }
            //assetType
            itemNodeAssetType.InnerText = assetType;

            //assetName 
            itemNodeAssetName.InnerText = name;

            CmapNodeName.InnerText = location_TB.Text;
            //setFileName
            //if (isUsingFileName == true)
            //{
            //    if (fileName_TB.Text.Length > 0)
            //    {
            //        fileName = fileName_TB.Text;
            //    }
            //    else
            //    {
            //        fileName = "template";
            //    }
            //}

            itemNodeCreatedBy.InnerText = "Skylumz YTYP Creator";
        }

        //sets all variables
        private void SetVariables()
        {

            //set textname so its not null if not using exrernal TextureDictionaryName
            textureDictionaryName = name;
            
            
            //lodDist
            lodDist = lodDist_TB.Text;

            //flags
            flags = flags_TB.Text;

            //specialAttributes
            specialAttributes = specialAttributes_TB.Text;

            //bbMin
            I_bbminX = bbMinX_TB.Text;
            I_bbminY = bbMinY_TB.Text;
            I_bbminZ = bbMinZ_TB.Text;

            //bbMax
            I_bbmaxX = bbMaxX_TB.Text;
            I_bbmaxY = bbMaxY_TB.Text;
            I_bbmaxZ = bbMaxZ_TB.Text;

            //bbCentre
            I_centerX = bsCentreX_TB.Text;
            I_centerY = bsCentreY_TB.Text;
            I_centerZ = bsCentreZ_TB.Text;

            //bbRadius
            I_radius = bsRadius_TB.Text;

            //hdTextureDist
            hdTextureDist = hdTextureDist_TB.Text;

            //name
            name = name_TB.Text;

            //assetType
            assetType = assetType_TB.Text;
        }

        //Imports all bb info from odrbrowser
        private void ImportBBs()
        {
            if (odrbrowser.bbIsCalculated == true)
            {
                //Imports BBmin's
                I_bbminName = odrbrowser.bbminName;
                I_bbminX = odrbrowser.bbminX;
                I_bbminY = odrbrowser.bbminY;
                I_bbminZ = odrbrowser.bbminZ;

                bbMinX_TB.Text = I_bbminX;
                bbMinY_TB.Text = I_bbminY;
                bbMinZ_TB.Text = I_bbminZ;

                //Imports BBmax's
                I_bbmaxName = odrbrowser.bbmaxName;
                I_bbmaxX = odrbrowser.bbmaxX;
                I_bbmaxY = odrbrowser.bbmaxY;
                I_bbmaxZ = odrbrowser.bbmaxZ;

                bbMaxX_TB.Text = I_bbmaxX;
                bbMaxY_TB.Text = I_bbmaxY;
                bbMaxZ_TB.Text = I_bbmaxZ;

                //Imports Center's
                I_centerName = odrbrowser.centerName;
                I_centerX = odrbrowser.centerX;
                I_centerY = odrbrowser.centerY;
                I_centerZ = odrbrowser.centerZ;

                bsCentreX_TB.Text = I_centerX;
                bsCentreY_TB.Text = I_centerY;
                bsCentreZ_TB.Text = I_centerZ;

                //Import Radius
                I_radiusName = odrbrowser.radiusName;
                I_radius = odrbrowser.radiusF;
                bsRadius_TB.Text = I_radius;

                double lds;
                double ir = double.Parse(I_radius, System.Globalization.CultureInfo.InvariantCulture);
                lds = 100 + (1.6 * ir);
                lodDist_TB.Text = lds.ToString();

                double ldd = lds * 0.65;
                hdTextureDist_TB.Text = ldd.ToString();

                //Import Model Name 
                I_modelName = odrbrowser.modelName;

                name_TB.Text = I_modelName.Replace("	", "");
            }
            else
            {
                MessageBox.Show("BB's are not calculated", "Error");
            }
        }

        //Opens odr browser
        private void OpenOdrBrowser()
        {
            odrbrowser f = new odrbrowser();
            f.Show();
        }

        //sets if using texture dictionary
        private void UsingTextureDictionary()
        {
            if (textureDictionary_TB.Visible == false)
            {
                textureDictionary_TB.Visible = true;
                isUsingTextureDict = true;

                textureDictionaryName = textureDictionary;
            }
            else if (textureDictionary_TB.Visible == true)
            {
                textureDictionary_TB.Visible = false;
                isUsingTextureDict = false;
            }
        }

        //sets if using physics dictionary
        private void UsingPhysicsDictionary()
        {
            if (isUsingPhysicsDict == false)
            {
                isUsingPhysicsDict = true;
                embeddedCols_TB.Visible = false;
            }
            else if (isUsingPhysicsDict == true)
            {
                isUsingPhysicsDict = false;
                embeddedCols_TB.Visible = true;
            }
        }

        //sets if using drawable dictionary
        private void UsingDrawableDictionary()
        {
            if (drawableDictionary_TB.Visible == false)
            {
                drawableDictionary_TB.Visible = true;
                isUsingDrawableDict = true;
            }
            else if (drawableDictionary_TB.Visible == true)
            {
                drawableDictionary_TB.Visible = false;
                isUsingDrawableDict = false;
            }
        }

        //sets if using custom file name
        private void UsingCustomFileName()
        {
            if (isUsingFileName == false)
            {
                isUsingFileName = true;
            }
            else if (isUsingFileName == true)
            {
                isUsingFileName = false;
            }
        }

        public void CreateData()
        {
            try
            {
                tempDoc.Load(Application.StartupPath + "\\Resources\\templateYTYPITEM.xml");

                SetVariables();

                CreateItemNode();

                //fileName = name;
                SaveXML();
            }
            catch (XmlException ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void ShowViewCreatedYtypButton()
        {
            if(createdYtyp == true)
            {
                ViewCreatedYtypButton.Visible = true;
                editYTYP.filePathI = filePathSavedTo;
                editYTYP.openedFromCreator = true;
                editYTYP.fileName = filePathSavedTo;
            }
        }

        private void ShowEnterFileName()
        {
            Enter_File_Name f = new Enter_File_Name();
            f.Show();
        }


        /*
         *  Creater Of This Program Is Skylumz
         *  https://www.gta5-mods.com/users/skylumz
         *  Version 1.0
        */


        //Creates YTYP ITEM
        private void createButton_Click(object sender, EventArgs e)
        {
            CreateData();
        }

        //Open odr browser
        private void openOdrBrowser_Click(object sender, EventArgs e)
        {
            OpenOdrBrowser();
        }

        //Creates new item node WIP
        private void newItem_Click(object sender, EventArgs e)
        {
            MessageBox.Show("Soon to be added!", "Error");
        }

        //Sets texturedictionary variable
        private void isUsingTextureDictionary_CheckedChanged(object sender, EventArgs e)
        {
            UsingTextureDictionary();
        }

        //Sets drawabledictionary variable
        private void isUsingDrawableDictionary_CheckedChanged(object sender, EventArgs e)
        {
            UsingDrawableDictionary();
        }

        //Sets physicsdictionary variable
        private void isUsingPhysicsDictionary_CheckedChanged(object sender, EventArgs e)
        {
            UsingPhysicsDictionary();
        }

        //Sets isUsingCustomFileName
        private void isUsingCustomFile_CheckedChanged(object sender, EventArgs e)
        {
            if (isUsingFileName == false)
            {
                ShowEnterFileName();
                isUsingFileName = true;
            }
            else if(isUsingFileName == true)
            {
                isUsingFileName = false;
                fileName = "template";
            }
        }

        //Imports BBS
        private void importBBsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            ImportBBs();
        }

        //Load editYTYP form
        private void ViewCreatedYtypButton_Click(object sender, EventArgs e)
        {
            editYTYP f = new editYTYP();
            f.Show();
        }

        private void Create_Load(object sender, EventArgs e)
        {

        }
    }
}
