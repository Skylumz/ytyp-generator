﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace createYTYP
{
    public partial class odrbrowser : Form
    {
        public string bbmin;
        public string bbmax;
        public string center;
        public string radius;
        public string model;

        public bool odrIsImported = false;
        public static bool bbIsCalculated = false;

        public static string bbminName;
        public static string bbminX;
        public static string bbminY;
        public static string bbminZ;

        public static string bbmaxName;
        public static string bbmaxX;
        public static string bbmaxY;
        public static string bbmaxZ;

        public static string centerName;
        public static string centerX;
        public static string centerY;
        public static string centerZ;

        public static string radiusName;
        public static string radiusF;

        public static string modelName;

        OpenFileDialog openFile = new OpenFileDialog();
        string line = "";

        public odrbrowser()
        {
            InitializeComponent();
        }

        private void odrbrowser_Load(object sender, EventArgs e)
        {
            openFile.Filter = "Odr Files (.odr)| *.odr";
        }

  
        private void fileToolStripMenuItem_Click(object sender, EventArgs e)
        {
            OpenODR();
        }

        //Opens.odr
        private void closeToolStripMenuItem_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void OpenODR()
        {
            odrIsImported = true;

            if (openFile.ShowDialog() == DialogResult.OK)
            {
                StreamReader sr = new StreamReader(openFile.FileName);
                while (line != null)
                {
                    line = sr.ReadLine();
                    if (line != null)
                    {
                        listBox1.Items.Add(line);
                    }
                }
                sr.Close();
            }
        }

        //show bbmin after its calculated
        private void bBMinToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if(bbIsCalculated == true)
            {
                MessageBox.Show(bbmin);
            }
            else
            {
                MessageBox.Show("Need to calculate BB's", "Error");
            }
        }

        //show bbmax after its calculated
        private void bBMaxToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (bbIsCalculated == true)
            {
                MessageBox.Show(bbmax);
            }
            else
            {
                MessageBox.Show("Need to calculate BB's", "Error");
            }
        }

        //show center after its calculated
        private void centerToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (bbIsCalculated == true)
            {
                MessageBox.Show(center);
            }
            else
            {
                MessageBox.Show("Need to calculate BB's", "Error");
            }
        }

        //show radius after its calculated
        private void radiusToolStripMenuItem_Click(object sender, EventArgs e)
        {
            if (bbIsCalculated == true)
            {
                MessageBox.Show(radius);
            }
            else
            {
                MessageBox.Show("Need to calculate BB's", "Error");
            }
        }

        private void caculateBBsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            calculateBB();
        }

        //calculates BB bt finding the strings
        private void calculateBB()
        {
            if(odrIsImported == true)
            {
                bbIsCalculated = true;

                string searchString1 = "AABBmin";
                string searchString2 = "AABBmax";
                string searchString3 = "Center";
                string searchString4 = "Radius";
                string searchString5 = "mesh";

                for (int i = 0; i < listBox1.Items.Count; i++)
                {
                    if (listBox1.Items[i].ToString().IndexOf(searchString1, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        listBox1.SetSelected(i, true);
                        bbmin = listBox1.SelectedItem.ToString();
                    }
                    else
                    {
                        // Do this if you want to select in the ListBox only the results of the latest search.
                        listBox1.SetSelected(i, false);
                    }
                    if (listBox1.Items[i].ToString().IndexOf(searchString2, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        listBox1.SetSelected(i, true);
                        bbmax = listBox1.SelectedItem.ToString();
                    }
                    else
                    {
                        // Do this if you want to select in the ListBox only the results of the latest search.
                        listBox1.SetSelected(i, false);
                    }
                    if (listBox1.Items[i].ToString().IndexOf(searchString3, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        listBox1.SetSelected(i, true);
                        center = listBox1.SelectedItem.ToString();
                    }
                    else
                    {
                        // Do this if you want to select in the ListBox only the results of the latest search.
                        listBox1.SetSelected(i, false);
                    }
                    if (listBox1.Items[i].ToString().IndexOf(searchString4, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        listBox1.SetSelected(i, true);
                        radius = listBox1.SelectedItem.ToString();
                    }
                    else
                    {
                        // Do this if you want to select in the ListBox only the results of the latest search.
                        listBox1.SetSelected(i, false);
                    }
                    if (listBox1.Items[i].ToString().IndexOf(searchString5, StringComparison.OrdinalIgnoreCase) >= 0)
                    {
                        listBox1.SetSelected(i, true);
                        model = listBox1.SelectedItem.ToString();
                    }
                    else
                    {
                        // Do this if you want to select in the ListBox only the results of the latest search.
                        listBox1.SetSelected(i, false);
                    }
                }
            }
            else
            {
                MessageBox.Show("Need to import .odr to calculate BB's", "Error");
            }
            if(odrIsImported == true)
            {
                SplitStrings();
            }
        }

        //splits strings so they are seperate
        public void SplitStrings()
        {
            //string modelCombo;  WORK IN PROGRESS

            //Split BBmin
            var bbminparts = bbmin.Split(new[] { ' ' });
            bbminName = bbminparts[0];
            bbminX = bbminparts[1];
            bbminY = bbminparts[2];
            bbminZ = bbminparts[3];

            //Split BBmax
            var bbmaxparts = bbmax.Split(new[] { ' ' });
            bbmaxName = bbmaxparts[0];
            bbmaxX = bbmaxparts[1];
            bbmaxY = bbmaxparts[2];
            bbmaxZ = bbmaxparts[3];

            //Split Center
            var centerparts = center.Split(new[] { ' ' });
            centerName = centerparts[0];
            centerX = centerparts[1];
            centerY = centerparts[2];
            centerZ = centerparts[3];

            //Split Center
            var radiusparts = radius.Split(new[] { ' ' });
            radiusName = radiusparts[0];
            radiusF = radiusparts[1];

            //splits Model 
            var modelparts = model.Split(new[] { '.' });
            string modelCombo = modelparts[0];
            string modelMesh = modelparts[1];                       
            //split modelcombo
            var modelcomboParts = modelCombo.Split(new[] { '\\' });
            modelName = modelcomboParts[0];
            string modelParent = modelcomboParts[1];
            modelName = modelName.Replace("	", "");
        }
    }
}
